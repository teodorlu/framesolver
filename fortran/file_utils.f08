!	file_utils.f08
!	Teodor Lunaas Heggelund
!
!	Prerequisites:	Valid file input
!
!	Description:		Contains subroutines for reading from and writing to files.
!
!									skip_lines skips n lines in a given file.
!
!									read_input reads an input file of the format defined in
!									bin/sampleframe.tab.
!
!									persist_output writes analysis data to "analysis-results.tab".
!									Information about which beams are connected is stripped away,
!									as it is not strictly needed by the visualizer.

module file_utils
	implicit none
	private :: skip_lines
	public  :: read_input
	logical :: debug = .false.
contains
	subroutine skip_lines(file_number, n)
		integer :: file_number, n, i
		character(len=20) :: line
		do i=1,n
			read(file_number, *), line
			if ( debug ) then
				print *, "Skipping line staring with '", line, "'"
			end if
		end do
	end subroutine skip_lines

	subroutine read_input(num_nodes, num_beams, node_locations, node_bc, loads, beams, beam_properties, file_name)
		! Output
		integer								:: num_nodes, num_beams
		real, allocatable			:: node_locations(:,:)
		integer, allocatable	:: node_bc(:,:), beams(:,:)
		real, allocatable			:: beam_properties(:,:), loads(:)
		character(len=16)			:: file_name

		integer 				:: i=0, j=0, file_number=7, factor

		open(unit=file_number, file=file_name)
		read(file_number, *), num_nodes

		allocate(node_locations(num_nodes, 2))
		allocate(node_bc(num_nodes, 3))
		allocate(loads(num_nodes * 3))

		! Reading lines manually to avoid transposing the matrix
		do i=1,num_nodes
			read (file_number,*) node_locations(i,1), node_locations(i,2)
			if ( debug ) then
				print *, "Found:", node_locations(i,1), node_locations(i,2)
			end if
		enddo

		call skip_lines(file_number, 2)
		do i=1,num_nodes
			read (file_number,*) node_bc(i,1), node_bc(i,2), node_bc(i,3)
		enddo

		call skip_lines(file_number, 2)
		do i=1,num_nodes
			factor = i-1
			read (file_number,*) loads(factor * 3 + 1), loads(factor * 3 + 2), loads(factor * 3 + 3)
		enddo

		call skip_lines(file_number, 1)
		read(file_number,*) num_beams
		allocate(beams(num_beams, 2))
		do i=1,num_beams
			read (file_number,*) beams(i,1), beams(i,2)
		end do

		call skip_lines(file_number, 2)
		allocate(beam_properties(num_beams, 3))
		do i=1,num_beams
			read (file_number,*) beam_properties(i,1), beam_properties(i,2), beam_properties(i,3)
		end do

		close(file_number)

		if (debug) then
			print *, node_locations
			print *, "---"
			print *, node_bc
			print *, "---"
			print *, beams
			print *, "---", size(beam_properties,1)
			print *, beam_properties
		end if

	end subroutine read_input

	subroutine persist_output(node_locations, local_loads, beams, num_nodes, num_beams)
		real,    intent(in) :: node_locations(:,:), local_loads(:,:)
		integer, intent(in) :: beams(:,:)
		integer, intent(in) :: num_nodes, num_beams

		real, allocatable	:: beam_node_locations(:,:)
		real 	:: pos1(2), pos2(2)
		integer :: i, file_number = 8

		allocate(beam_node_locations(num_beams, 4)) ! Each row contains (/x0 y0 x1 y1/)

		do i = 1, num_beams
			pos1 = node_locations(beams(i,1), :)
			pos2 = node_locations(beams(i,2), :)
			beam_node_locations(i, 1:2) = pos1
			beam_node_locations(i, 3:4) = pos2
		end do

		open(unit=file_number, file="analysis-results.tab")
		
		write(file_number, *) "Number of beams:"
		write(file_number, *) num_beams

		write(file_number, *) "Node locations for each beam:"
		do i = 1, num_beams
			write(file_number, *) beam_node_locations(i, :)
		end do

		write(file_number, *) "Resulting force data:"
		do i = 1, num_beams
			write(file_number, *) local_loads(i, :)
		end do
		write(file_number, *)

		close(file_number)

	end subroutine persist_output

end module file_utils

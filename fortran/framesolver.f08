!	framesolver.f08
! Teodor Lunaas Heggelund
!
! Prerequisites:  None
!
! Description:    Framesolver program. Uses other modules for logic.
!									
!									If a command line parameter is present, the file at that
!									location is used. Otherwise, it reverts to sample input
!									in "sampleframe.tab".
!
!									Framesolver then reads input, assembles a global stiffness
!									matrix, and sets up the stiffness equation. Solves the equation
!									using gauss-seidel, and backwards substitutes to find the
!									beam loads.
!
!									Output is written to "analysis-results.tab", in the same folder
!									
!	Note:						I left some outcommented code below that may aid in understanding
!									the control flow and data format.
!
!									Code is indended to be read using 2 spaces per tab, as defined in
!									framesolver.sublime-project.


program framesolver
	use file_utils
	use stiffness_matrix_utils
	use system_analysis_utils
	use gauss_seidel_solver
	implicit none

	integer								:: num_nodes, num_beams
	real, allocatable 		:: node_locations(:,:), global_stiffness_matrix(:,:), beam_properties(:,:)
	real, allocatable 		:: loads(:), dofs(:)
	integer, allocatable 	:: node_bc(:,:), beams(:,:)
	real, allocatable 		:: local_loads(:,:)

	integer 							:: num_args
	character(len=32)			:: file_name

	print *, "Framesolver starting up"

	num_args = command_argument_count()
	if ( num_args .gt. 0 ) then
	 	call get_command_argument(1,file_name)
	else
		file_name = "sampleframe.tab"
	end if
	print *, "Reading input from '" // trim(file_name) // "'"

	call read_input(num_nodes, num_beams, node_locations, node_bc, loads, beams, beam_properties, file_name)

	global_stiffness_matrix = assemble_global_stiffness_matrix(node_locations, beams, beam_properties, num_nodes, num_beams)

! 	call pretty_print_matrix(global_stiffness_matrix)
! 	print *, "///"
	call insert_boundary_conditions(global_stiffness_matrix, node_bc, num_nodes)

! 	call pretty_print_matrix(global_stiffness_matrix)
! 	print *, "///"

	dofs = linear_system_solver(global_stiffness_matrix, loads, 0.000001)

! 	print *, loads
! 	print *, "///"

	local_loads = calculate_resulting_loads(dofs, node_locations, beams, beam_properties, num_nodes, num_beams)

! 	print *, "/// ", "Global displacements:"
! 	print *, dofs
! 	print *, "///"
! 	call pretty_print_matrix(local_loads)
	
	call persist_output(node_locations, local_loads, beams, num_nodes, num_beams)

! 	call pretty_print_matrix(global_stiffness_matrix)
	print *, "Framesolver done"

end program

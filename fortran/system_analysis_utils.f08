!	system_analysis_utils.f08
!	Teodor Lunaas Heggelund
!
!	Prerequisites:	Stiffness matrix util module for providing local stiffness
! 								matrix generation.
!
!	Description:		Contains functions and subroutines for handling the
!									system's stiffness matrix
!
!									Features include global stiffness matrix assembly,
!									boundary condition insertion and backwards substitution
!									for finding resulting beam forces.

module system_analysis_utils
	use stiffness_matrix_utils
	implicit none
	private	:: vector_length_2d
	public	:: assemble_global_stiffness_matrix, insert_boundary_conditions, calculate_resulting_loads
	logical :: debug = .false.
contains
	function assemble_global_stiffness_matrix(node_locations, beams, properties, num_nodes, num_beams) result(global_stiffness_matrix)
! 		Iterates over all beams, and adds their local stiffness matrices to the global stiffness matrix
! 		num_beams and num_nodes are not strictly necessary, but remove the need to allocate later
		implicit none
		integer, intent(in)	:: num_nodes, num_beams
		real,    intent(in)	:: node_locations(num_nodes, 2)	! x, y for each node
		integer, intent(in)	:: beams(num_beams, 2)			! fromID, toID for each node
		real,    intent(in) :: properties(:, :)				! E A Iz
		
		real								:: global_stiffness_matrix(num_nodes*3, num_nodes*3)	! 3 dofs (x, y, theta) for each node
		integer							:: i, n, from_node, to_node, from_node_index, to_node_index, f, t
		real								:: L, angle, E, A, Iz
		real, dimension(2)	:: pos1, pos2, delta
		real								:: rotated_local_stiffness(6,6)

		n  = num_nodes * 3
		global_stiffness_matrix(:,:) = 0.

! 		call pretty_print_matrix(global_stiffness_matrix)

! 		Add contributions from each of the beams
		do i=1,num_beams
			from_node = beams(i, 1)
			to_node = beams(i, 2)
! 			print *, "Beam", i , "from", from_node, "to", to_node

			pos1 = node_locations(from_node, :)
			pos2 = node_locations(to_node  , :)
			delta = pos2 - pos1
			L = vector_length_2d(delta)

			E  = properties(i, 1)
			A  = properties(i, 2)
			Iz = properties(i, 3)
			rotated_local_stiffness = rotated_stiffness_matrix(E, A, Iz, L, delta)

! 			3 dofs per node, and indexing starting on 1
			from_node_index = (from_node - 1) * 3 + 1
			to_node_index   = (to_node   - 1) * 3 + 1

			f = from_node_index
			t = to_node_index
			
			if (debug) then
				print *, "///", f, t
				call pretty_print_matrix(rotated_local_stiffness)
			end if

! 			Place local stiffness matrices in the correct postitions
			global_stiffness_matrix(f:f+2, f:f+2) = global_stiffness_matrix(f:f+2, f:f+2) + rotated_local_stiffness(1:3, 1:3)
			global_stiffness_matrix(t:t+2, f:f+2) = global_stiffness_matrix(t:t+2, f:f+2) + rotated_local_stiffness(4:6, 1:3)
			global_stiffness_matrix(f:f+2, t:t+2) = global_stiffness_matrix(f:f+2, t:t+2) + rotated_local_stiffness(1:3, 4:6)
			global_stiffness_matrix(t:t+2, t:t+2) = global_stiffness_matrix(t:t+2, t:t+2) + rotated_local_stiffness(4:6, 4:6)
			
			if (debug) then
				print *, "==="
				call pretty_print_matrix(global_stiffness_matrix)
			end if

		end do

	end function assemble_global_stiffness_matrix

	subroutine insert_boundary_conditions(stiffness, node_bc, num_nodes)
		implicit none
		integer			:: num_nodes, i, j, node_index
		real 				:: stiffness(num_nodes*3, num_nodes*3)
		integer			:: node_bc(num_nodes, 3)

		do i=1, num_nodes
			do j=1, 3
				if (node_bc(i, j) == 1) then
					node_index = (i-1)*3 + j
					stiffness(         :, node_index) = 0
					stiffness(node_index,          :) = 0
					stiffness(node_index, node_index) = 1.
				end if
			end do
		end do

	end subroutine insert_boundary_conditions

	function calculate_resulting_loads(dofs, node_locations, beams, properties, num_nodes, num_beams) result(local_loads)
		implicit none
		real 				:: dofs(num_nodes * 3)
		real 				:: node_locations(num_nodes, 2)
		integer 		:: num_nodes, num_beams
		integer 		:: beams(num_beams, 2)
		real 				:: properties(num_beams, 3)
		real 				:: local_stiffness(6,6)
		real 				:: local_loads(num_beams, 6)	! S forces on each beam

		integer 		:: i, from_node, to_node
		integer 		:: f, t												! From and to node indices
		real 				:: E, A, Iz, L
		real 				:: delta(2)
	
		real 				:: r(6)												! Unrotated displacements for local beam element
		real 				:: v(6), S(6)									! Local displacements
		real 				:: rotation_matrix(6,6)

		local_loads(:,:) = 0.

		do i=1,num_beams
			from_node = beams(i,1)
			to_node = beams(i,2)
			delta = node_locations(from_node, :) - node_locations(to_node, :)

			L  = vector_length_2d(delta)
			E  = properties(i, 1)
			A  = properties(i, 2)
			Iz = properties(i, 3)

			local_stiffness = generate_stiffness_matrix(E, A, Iz, L)

			f = 1 + (from_node - 1) * 3
			t = 1 + (to_node - 1) * 3

			r(1:3) = dofs(f:f+2)
			r(4:6) = dofs(t:t+2)

			rotation_matrix = rotation_transformation_6dof(delta)

			v = matmul(rotation_matrix, r)
			local_loads(i, :) = matmul(local_stiffness, v)
		end do
		
	end function calculate_resulting_loads

	function vector_length_2d(vector) result(length)
		implicit none
		real, intent(in)	:: vector(2)
		real							:: length
		
		length = sqrt(vector(1)*vector(1) + vector(2)*vector(2))
	end function vector_length_2d

end module system_analysis_utils

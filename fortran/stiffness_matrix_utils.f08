!	stiffness_matrix_utils.f08
!	Teodor Lunaas Heggelund
!
!	Prerequisites:	None
!
!	Description:		Contains functions and subroutines for handling element
!									stiffness matrices. 
!
!									Features include local stiffness matrix generation, 
!									stiffness matrix rotation and readable matrix output
!									to console.
!
!									Currently uses functions for all matrix handling.
!									Room for optimization: use subroutines, and avoid having
!									to allocate new memory for each time a matrix is to be
!									returned.

module stiffness_matrix_utils
	implicit none
	private :: mult_div, rotation_transformation_3dof
	public  :: generate_stiffness_matrix, pretty_print_matrix, rotation_transformation_6dof
contains
	function mult_div(f1, f2, div) result(product)
		implicit none
		real, intent(in)	:: f1, f2, div
		real				:: product
		product = f1 * f2 / div
	end function mult_div

	function generate_stiffness_matrix(E, A, Iz, L) result(stiffness)
		implicit none
		real, intent(in)	:: E, A, L, Iz
		real				:: eal, eil
		real				:: stiffness(6,6), test_k(2,2)
		eal = mult_div(E, A, L)
		eil = mult_div(E, Iz, L*L*L)

! 		Setting columns
		stiffness(:,1) = (/1., 0., 0., -1., 0., 0./)*eal
		stiffness(:,2) = (/0., 12.0, -6*L, 0., -12.0, -6*L/)*eil
		stiffness(:,3) = (/0., -6*L, 4*L*L, 0., 6*L, 2*L*L/)*eil

		stiffness(:,4) = (/-1., 0., 0., 1., 0., 0./)*eal
		stiffness(:,5) = (/0., -12.0, 6*L, 0., 12.0, 6*L/)*eil
		stiffness(:,6) = (/0., -6*L, 2*L*L, 0., 6*L, 4*L*L/)*eil

! 		call pretty_print_matrix(rotation_transformation_3dof(3.14/2))
! 		print *, matmul(stiffness, stiffness)
	end function generate_stiffness_matrix

	subroutine pretty_print_matrix(matrix)
		intent(in)	:: matrix
		real		:: matrix(:, :)
		integer		:: m, n, i, j
		m = size(matrix, 1)
		n = size(matrix, 2)

		do, i=1,m
			print "(100g15.5)", ( matrix(i,j), j=1,n )
		enddo
	end subroutine pretty_print_matrix

	function rotation_transformation_3dof(dir) result(rotation)
		implicit none
		real, intent(in)	:: dir(2)
		real				:: rotation(3,3), nx, ny, L

		L  = sqrt(dir(1)*dir(1) + dir(2)*dir(2))
		nx = dir(1)/L
		ny = dir(2)/L
		
		rotation(:,1) = (/ nx, ny, 0. /)
		rotation(:,2) = (/-ny, nx, 0. /)
		rotation(:,3) = (/ 0., 0., 1. /)

	end function rotation_transformation_3dof

	function rotation_transformation_6dof(direction) result(rotation)
		implicit none
		real, intent(in)	:: direction(2)
		real				:: rotation(6,6)
		real				:: small_rotation(3,3)
		
		small_rotation = rotation_transformation_3dof(direction)
		rotation(1:3,1:3) = small_rotation
		rotation(4:6,4:6) = small_rotation
		rotation(4:6,1:3) = 0.
		rotation(1:3,4:6) = 0.
	end function rotation_transformation_6dof

	function rotated_stiffness_matrix(E, A, Iz, L, direction) result(rotated_stiffness)
! 		TODO: Go back from direction, and use the beam's normal vector instead
		implicit none
		real, intent(in)	:: E, A, L, Iz, direction(2)
		real				:: rotated_stiffness(6,6), rotation(6,6), stiffness(6,6)

		rotation = rotation_transformation_6dof(direction)
		stiffness = generate_stiffness_matrix(E, A, Iz, L)

		rotated_stiffness = matmul(matmul(transpose(rotation), stiffness), rotation)
	end function rotated_stiffness_matrix

end module stiffness_matrix_utils

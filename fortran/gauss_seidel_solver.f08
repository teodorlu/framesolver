!	gauss_seidel_solver.f08
!	Teodor Lunaas Heggelund
!
!	Prerequisites:	None
!
!	Description:		A linear equation solver based on the gauss-seidel 
!									algorithm. Iterative: not exact, but potentially
!									faster on large data sets.

module gauss_seidel_solver
	implicit none
	public :: linear_system_solver, linear_system_solver_test
contains
	function linear_system_solver(A, b, error) result(x)
		implicit none
		real, intent(in)	:: A(:,:), b(:), error
		real, allocatable	:: x(:), residual(:)
		integer						:: n, iteration, i, j
		real							:: sigma

		n = size(b)
		allocate(x(n))
		allocate(residual(n))

		x(:) = 0.

		do iteration = 1, 1000
			do i = 1, n
				sigma = 0.
				do j = 1, n
					if ( j /= i ) then
						sigma = sigma + A(i, j) * x(j)
					end if
				end do
				x(i) = (b(i)-sigma)/A(i, i)
			end do
			residual = matmul(A, x) - b
			if ( maxval(abs(residual)) < error ) exit
		end do

	end function linear_system_solver

	subroutine linear_system_solver_test
		real :: A(2,2), b(2), x(2)
		A(:,1) = (/ 16.,   7./)
		A(:,2) = (/  3., -11./)
		b 	   = (/ 11.,  13./)

		x = linear_system_solver(A, b, 0.001)
	end subroutine linear_system_solver_test
	
end module gauss_seidel_solver

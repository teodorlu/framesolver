/*
  visualizer.c
  Teodor Lunaas Heggelund
  
  Prerequisites:  OpenGL and GLUT.
                  for compiling on Ubuntu Linux and Debian Linux, install using
                  sudo apt-get install freeglut3 freeglut3-dev libglew1.5 libglew1.5-dev libglu1-mesa libglu1-mesa-dev libgl1-mesa-glx libgl1-mesa-dev

  Description:    Reads result data from framesolver.
                  Visualises with OpenGL and GLUT.

  Usage:          Follow on screen instructions.
*/

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "GL/glut.h"

int numBeams;
int debug = 0;
float **nodePos;  // Node coordinates for each beam
float **loads;    // Nodal loads for each beam
float maxLoad[3];

GLuint Window = 0;

// N, V, M defined compile time -> can be used in swich case.
#define N 0
#define V 1
#define M 2

unsigned short int currentLoad;
float scale = 1.0;
float maxColor = 0.;

void readInput(void)
{
  FILE *inputf;
  int bufSize = 1024;
  char buffer[bufSize];
  int i;

  inputf = fopen("analysis-results.tab", "r");

  fgets(buffer, bufSize, inputf); // Number of beams:
  fscanf(inputf, "%i", &numBeams);

  if (!numBeams)
    exit(1);
  fgets(buffer, bufSize, inputf); // Skip the rest of numBeams line

  nodePos = (float**) malloc(sizeof(float*)*numBeams);
  fgets(buffer, bufSize, inputf); // Node locations for each beam:
  for (i = 0; i < numBeams; ++i)
  {
    nodePos[i] = malloc(sizeof(float) * 4);
    fscanf(inputf, "%f %f %f %f",
      &nodePos[i][0], &nodePos[i][1], &nodePos[i][2], &nodePos[i][3]);
  }

  // nodePos = nodeInput;
  fgets(buffer, bufSize, inputf); // Skip the rest of the last nodePos line

  loads = (float**) malloc(sizeof(float*)*numBeams);
  fgets(buffer, bufSize, inputf); // Resulting force data:
  for (i = 0; i < numBeams; ++i)
  {
    loads[i] = malloc(sizeof(float) * 6);
    fscanf(inputf, "%f %f %f %f %f %f",
      &loads[i][0], &loads[i][1], &loads[i][2], &loads[i][3], &loads[i][4], &loads[i][5]);
  }
  // loads = loadInput;
}

void printState(void)
{
  int i;
  for (i = 0; i < numBeams; ++i)
  {
    printf("Beam %d:\n", i);

    printf("  Nodal coordinates -- %f %f %f %f\n",
      nodePos[i][0], nodePos[i][1], nodePos[i][2], nodePos[i][3]
      );

    printf("  Nodal loads       -- %f %f %f %f %f %f\n",
      loads[i][0], loads[i][1], loads[i][2], loads[i][3], loads[i][4], loads[i][5]
      );
  }
}

float findCenter(int x)
{
  if (x != 0 && x != 1)
    exit(3);

  int row, col;
  float min, max;
  min = nodePos[0][x];
  max = nodePos[0][x];

  for (row = 0; row < numBeams; ++row)
  {
    for (col = x; col < 4; col += 2)
    {
      if (nodePos[row][col] < min)
        min = nodePos[row][col];
      if (nodePos[row][col] > max)
        max = nodePos[row][col];      
    }
  }

  return (min + max)/2.0;
}

void addToCoordinates(int x, float f)
{
  if (x != 0 && x != 1)
    exit(3);

  int row, col;
  for (row = 0; row < numBeams; ++row)
  {
    for (col = x; col < 4; col += 2)
    {
      nodePos[row][col] += f;
    }
  }
}

void centerOrigo(void)
{
  float centerX = findCenter(0);
  float centerY = findCenter(1);
  addToCoordinates(0, -centerX);
  addToCoordinates(1, -centerY);
}

float maxCoordinate(void)
{
  int i, j;
  float max = 0;
  for (i = 0; i < numBeams; ++i)
  {
    for (j = 0; j < 4; ++j)
    {
      if(nodePos[i][j] > max)
        max = nodePos[i][j];
    }
  }
  return max;
}

void scaleCoordinates(float factor)
{
  int i, j;
  for (i = 0; i < numBeams; ++i)
  {
    for (j = 0; j < 4; ++j)
    {
      nodePos[i][j] *= factor;
    }
  }
}

void convertToRelativeCoordinates(void)
{
  centerOrigo();
  float extreme = maxCoordinate();
  scaleCoordinates(0.7 / extreme);
}

float fmaxabs(float a, float b)
{
  if (fabsf(a) > fabsf(b)) 
    return a;
  else
    return b;
}

void findMaxLoads(void)
{
  maxLoad[N] = 0.;
  maxLoad[V] = 0.;
  maxLoad[M] = 0.;

  int i, j;
  for (i = 0; i < numBeams; ++i)
  {
    for (j = 0; j < 2; ++j)
    {
      maxLoad[N] = fmaxabs(maxLoad[N], loads[i][N + j*3]);
      maxLoad[V] = fmaxabs(maxLoad[V], loads[i][V + j*3]);
      maxLoad[M] = fmaxabs(maxLoad[M], loads[i][M + j*3]);
    }
  }
  printf("Found max loads: N = %5.4f, V = %5.4f, M = %5.4f.\n",
                   maxLoad[N],maxLoad[V],maxLoad[M]);
}

void setScale(float limit)
{
  scale = limit;
}

float scaled(float f)
{
  return f / scale;
}

void colorizeFractionMod(float f)
{
  if (f > 0.)
    glColor3f(f, 0.0f, 0.0f); // red scale
  else
    glColor3f(0.0f, 0.0f, f); // blue
}

void colorizeFraction(float f)
{
  f = f/2;
  glColor3f(0.5f + f, 0.0f, 0.5f - f); // red scale
}

void drawBeams(void)
{
  setScale(maxLoad[currentLoad]);
  glLineWidth(3);
  glBegin(GL_LINES);
    int i;
    for (i = 0; i < numBeams; ++i)
    {
      float fractionA = -scaled(loads[i][0 + currentLoad]);
      colorizeFraction(fractionA);
      glVertex2f(nodePos[i][0], nodePos[i][1]);

      float fractionB = scaled(loads[i][3 + currentLoad]);
      colorizeFraction(fractionB);
      glVertex2f(nodePos[i][2], nodePos[i][3]);

      maxColor = fmaxabs(maxColor, fractionA);
      maxColor = fmaxabs(maxColor, fractionB);
    }
  glEnd();
}

const char * describeCurrentLoad() {
  switch (currentLoad) {
    case N:
      return "normal force";
    case V:
      return "shear force";
    case M:
      return "moment";
    default:
      return "not set";
  }
}

const char * currentUnit() {
  switch (currentLoad) {
    case N:
      return "N";
    case V:
      return "N";
    case M:
      return "Nmm";
    default:
      return "unknown";
  }  
}

void drawRectangle(float x, float y, float dx, float dy)
{
  glBegin(GL_POLYGON);
    glVertex2f(x, y);
    glVertex2f(x + dx, y);
    glVertex2f(x + dx, y + dy);
    glVertex2f(x, y + dy);
  glEnd();
}

void drawInfo(void)
{
  glColor3f(0.0, 0.0, 0.0);
  char textBuffer[128];
  glRasterPos3f(-0.6, 0.8, 0.0);
  sprintf(textBuffer, "Framesolver result viewer");
  glutBitmapString(GLUT_BITMAP_HELVETICA_18, textBuffer);

  glColor3f(0.0, 0.0, 0.0);
  glRasterPos3f(-0.6, 0.75, 0.0);
  sprintf(textBuffer,
    "Showing %s. 'n', 'v' or 'm' changes view. 'l' logs to console. 'ESC' to quit.",
    describeCurrentLoad());
  glutBitmapString(GLUT_BITMAP_HELVETICA_12, textBuffer);

  colorizeFraction(maxColor);
  drawRectangle(-.65, -.8, .03, .03);

  glColor3f(0.0, 0.0, 0.0);
  glRasterPos3f(-0.6, -0.8, 0.0);
  sprintf(textBuffer,
    "Maximum %s is %5.4f %s.",
    describeCurrentLoad(),
    maxLoad[currentLoad],
    currentUnit());
  glutBitmapString(GLUT_BITMAP_HELVETICA_12, textBuffer);

  colorizeFraction(0);
  drawRectangle(-.65, -.85, .03, .03);

  glColor3f(0.0, 0.0, 0.0);
  glRasterPos3f(-0.6, -0.85, 0.0);
  sprintf(textBuffer, "Zero %s", describeCurrentLoad());
  glutBitmapString(GLUT_BITMAP_HELVETICA_12, textBuffer);

}

void display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glColor3f(1.0f, 0.0f, 0.0f);
  drawBeams();
  drawInfo();
  glutSwapBuffers();
}

void keyPress(unsigned char key, int x, int y)
{
  switch (key) {
    case 'n':
      currentLoad = N;
      break;
    case 'v':
      currentLoad = V;
      break;
    case 'm':
      currentLoad = M;
      break;
    case 'l':
      printState();
      break;
    case 27:
      glutDestroyWindow(Window);
      exit(0);
  }

  printf("Pressed %c. Showing %s.\n", key, describeCurrentLoad());
  display();
  glutPostRedisplay();
}

void visualize()
{
  maxColor = 0.;
  glutInitWindowPosition(50, 50);
  glutInitWindowSize(700, 700);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE | GLUT_MULTISAMPLE);
  glEnable(GL_MULTISAMPLE);
  
  Window = glutCreateWindow("Framesolver visualizer");
  if (!Window) {
    exit(1);
  }

  glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
  
  glutDisplayFunc(display);
  glutKeyboardFunc(keyPress);
  
  glutMainLoop();
  // glutMainLoop never returns flow
}

int main(int argc, char *argv[])
{
  readInput();

  currentLoad = M;

  if (debug)
    printState();

  convertToRelativeCoordinates();
  findMaxLoads();

  glutInit(&argc, argv);
  visualize();
  exit(0);
}

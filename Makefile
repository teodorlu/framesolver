CLEANEXT=*.aux *.fls *.glo *.idx *.log *.toc *.ist *.acn *.acr *.alg *.bbl *.blg *.dvi *.glg *.gls *.ilg *.ind *.lof *.lot *.maf *.mtc *.mtc1 *.out *.synctex.gz *.fdb_latexmk

.PHONY: all clean veryclean run report
all:
	@cd fortran; make
	@cd c; make
clean:
	@cd fortran; make clean
	@cd c; make clean
	@rm -f bin/analysis-results.tab
	@cd report; rm -rf $(CLEANEXT)
veryclean: clean
	@rm -f bin/framesolver.out
	@rm -f bin/visualizer.out
	@rm -f report/report.pdf
run: bin/framesolver.out bin/visualizer.out
	@./bin/framesolver.out $(pwd)$(f)
	@cd bin; ./visualizer.out
bin/framesolver.out: all
bin/visualizer.out: all

report: report/report.pdf
	@cd report; xdg-open report.pdf
report/report.pdf: report/report.tex
	@cd report; latexmk -pdf report.tex 

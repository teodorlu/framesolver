Framesolver
===========

A tool for analysing static frame problems. Usage:
	
	make
	cd bin
	./run.sh beam.tab

or simply

	make run f=bin/frame.tab

Written in Fortran and C. Graphics with OpenGL.

This wouldn't have turned out nearly as well without help. Thanks to:

 - Håvard Holm for consulting on C and Fortran
 - Kolbein Bell and Kjell Kjell Magne Mathisen for consulting on theoretical mechanics
 - Arne Meuche for ninja makefile magic
 - Sigurd Holsen for how to properly allocate memory in C
